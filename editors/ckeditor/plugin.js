/**
 * @file Plugin for inserting embed code with wysiwyg_embed
 */
(function ($) {
  CKEDITOR.plugins.add('wysiwyg_embed', {

    requires: 'widget',

    init: function(editor) {

      // Declare a new Dialog
      CKEDITOR.dialog.add('wysiwyg_embed', this.path + 'dialog.js');

      // Declare a new widget.
      editor.widgets.add('wysiwyg_embed', {
        // Bind the widget to the Dialog command.
        dialog: 'wysiwyg_embed',

        // Declare the elements to be upcasted back.
        // Otherwise, the widget's code will be ignored.
        // Basically, we will allow all divs with 'leaflet_div' class,
        // including their alignment classes, and all iframes with
        // 'leaflet_iframe' class, and then include
        // all their attributes.
        // Read more about the Advanced Content Filter here:
        // * http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
        // * http://docs.ckeditor.com/#!/guide/plugin_sdk_integration_with_acf
        // allowedContent: 'div(!leaflet_div,align-left,align-right,align-center)[*];'
        //                     + 'iframe(!leaflet_iframe)[*];',

        // Declare the widget template/structure, containing the
        // important elements/attributes. This is a required property of widget.
        template:
          '<div id="" class="wysiwyg_embed_div"' +
          '           data-url=""              ' +
          '           data-width=""            ' +
          '           data-height=""           ' +
          '           data-alignment=""></div> '


      });

      // Add Button
      editor.ui.addButton('wysiwyg_embed', {
        label: 'Wysiwyg embed',
        command: 'wysiwyg_embed',
        icon: this.path + 'wysiwyg_embed.png'
      });

      // Add Command
      // editor.addCommand('video_filter', {
      //   exec : function () {
      //     var path = (Drupal.settings.video_filter.url.wysiwyg_ckeditor) ? Drupal.settings.video_filter.url.wysiwyg_ckeditor : Drupal.settings.video_filter.url.ckeditor
      //     var media = window.showModalDialog(path, { 'opener' : window, 'editorname' : editor.name }, "dialogWidth:580px; dialogHeight:480px; center:yes; resizable:yes; help:no;");
      //   }
      // });

      // Register an extra fucntion, this will be used in the popup.
      // editor._.video_filterFnNum = CKEDITOR.tools.addFunction(insert, editor);
    }

  });

})(jQuery);
