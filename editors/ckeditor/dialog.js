CKEDITOR.dialog.add('wysiwyg_embed', function(editor) {
  // Dialog's function callback for the Leaflet Widget.
  return {
    title: 'Embed external content',
    minWidth: 500,
    minHeight: 125,

    contents: [
      { // Create the input tab.
        id: 'input_tab',
        label: 'Url',
        elements: [
          {
            id: 'url',
            className: 'url',
            type: 'text',
            label: 'Url',

            setup: function(widget) {
              // Get value from widget
              if (widget.element.data('url') != "") {
                this.setValue(widget.element.data('url'));
              }

// var dialog = this.getDialog();
//     dialog.disableButton('ok');

              // var ckDialog = window.CKEDITOR.dialog.getCurrent();
              // var ckOk = ckDialog._.buttons['ok'];
              // console.log(ckOk);
            },

            onShow: function (widget) {

            },

            // This will execute every time you click the Dialog's OK button.
            commit: function(widget) {
              // Remove the content of the template div if it has one.
              widget.element.setHtml('');

              // Collect data from the input dialog
              var parameters = {
                url:       jQuery('.url input').val(),
                width:     jQuery('.width input').val(),
                height:    jQuery('.height input').val(),
                alignment: jQuery('.alignment select').val()
              };

              if (parameters['url'] == "") {
                alert("Geef een url in.")
              }

              // Set id of the widget
              var milliseconds = new Date().getTime();
              widget.element.setAttribute('id', 'wysiwyg_embed_div-' + milliseconds);

              // Set/Update the widget's data attributes and build request
              var request = document.location.protocol + '//' + document.domain + "/wysiwyg_embed?";
              for (var key in parameters) {
                widget.element.data(key, parameters[key]);
                request = request + key + "=" + parameters[key] + "&";
              }

              // Make request
              jQuery.getJSON(request, function(data) {
                if (data["error"] == 1) {
                  alert(data["error_msg"]);
                }
                else {
                  // Insert the embed code to the widget's DIV element.
                  widget.element.setHtml(data["embed_code"]);
                }
              });
            }
          }
        ]
      }, // end input tab


      { // Create an Options tab.
        id: 'options_tab',
        label: 'Options',
        elements: [
          { // Create a new horizontal group (for dimensions).
            type: 'hbox',
            // Set the relative widths of the fields fields.
            widths: [ '50%', '50%' ],
            children: [
              {
                id: 'width',
                className: 'width',
                type: 'text',
                label: 'Width',
                setup: function(widget) {
                  // Get value from widget
                  if (widget.element.data('width') != "") {
                    this.setValue(widget.element.data('width'));
                  }
                },
              },

              {
                id: 'height',
                className: 'height',
                type: 'text',
                label: 'Height',
                setup: function(widget) {
                  // Get value from widget
                  if (widget.element.data('height') != "") {
                    this.setValue(widget.element.data('height'));
                  }
                },
              },
            ]
          },

          { // Create a select list for Map Alignment.
            id: 'alignment',
            className: 'alignment',
            type: 'select',
            label: 'Alignment',
            items: [['Left', 'left'], ['Right', 'right'], ['Center', 'center']],

            // This will execute also every time you edit/double-click the widget.
            setup: function(widget) {
              // Set this map alignment's select list when
              // the current map has been initialized and set previously.
              if (widget.element.data('alignment') != "") {
                // Set the alignment.
                this.setValue(widget.element.data('alignment'));
              }
              // Set the Default alignment value.
              else {
                this.setValue("left");
              }
          },

        }
        ]
      } // end options tab
  ]};
});
