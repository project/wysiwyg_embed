Extra CKEditor plugins needed:

  - widget (http://ckeditor.com/addon/widget)
  - lineutils (http://ckeditor.com/addon/lineutils)

Installation:

  - Configure your site so you can create/edit content with ckeditor (configured with the wysiwyg module)
  - Enable the 'Wysiwyg embed button' (admin/config/content/wysiwyg)
  - Test the '</>' button

ToDo list:

  This module is, for now, a proof of concept.  A Lot of work needs to be done.
  Therefore, this todolist is not complete :-)

  - Create a text filter so saved content renders the url and other parameters
    provided in the widget and returns the embed code in a safe way.
    (for now, you only will see the result after saving when using full html and
    allowing iframes)

  - Better integration of video_filter module (or not using it).

  - Do not ignore parameters in the dialog to render the result.

  - Validate url in the widget dialog.

  - Better error handling (eg. do not place empty widget in the editor)

  - Sanitize input / security / ...

  - ...
